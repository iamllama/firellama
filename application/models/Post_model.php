<?php

/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 1:05 PM
 */
class Post_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_posts($slug = NULL)
	{
		if ($slug === NULL) {
			$this->db->order_by('posts.id', 'DESC');
			$query = $this->db->get('posts');
			return $query->result_array();
		}
		$this->db->join('categories', 'categories.id=posts.category_id');
		$query = $this->db->get_where('posts', array('slug' => $slug));
		return $query->row_array();
	}

	public function set_post()
	{
		$slug = url_title($this->input->post('title'));
		$data = array('title' => $this->input->post('title'),
			'slug' => $this->input->post('slug'),
			'body' => $this->input->post('body'),
			'excerpt' => $this->input->post('excerpt'),
			'category_id' => $this->input->post('category_id'),
		);
		return $this->db->insert('posts', $data);
	}

	public function delete_post($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('posts');
		return true;
	}

	public function update_post($id)
	{
		$slug = url_title($this->input->post('title'));
		$data = array('title' => $this->input->post('title'),
			'slug' => $slug,
			'body' => $this->input->post('body'),
			'excerpt' => $this->input->post('excerpt'),
			'category_id' => $this->input->post('category_id'),

		);
		$this->db->where('id', $this->input->post('id'));
		return $this->db->update('posts', $data);
	}

	public function get_categories(){
		$this->db->order_by('name');
		$query = $this->db->get('categories');
		return $query->result_array();
	}
}
