<?php echo form_open('users/login') ?>
<div style="border: 1px solid; padding: 5px;" class="container text-center">
	<div class="row" >
		<div class="col-12 col-sm-12 col-md-7 col-lg-7 text-center">
			<img alt="llama" src="<?php echo base_url(); ?>assets/img/lg.svg"
				 align="center"
				 class="img-fluid text-right">
		</div>
		<div style="margin: auto;" class="col-12 col-sm-12 col-md-5 col-lg-5">
			<h1>Let's get this bread</h1>
			<div class="form-group">
				<input class="form-control" type="text" name="username" placeholder="Username" autofocus>
			</div>
			<div class="form-group">
				<input class="form-control" type="password" name="password" placeholder="Password">
			</div>
			<button type="submit" name="submit" class="btn-outline-success btn btn-block">
				Login!
			</button>
		</div>
		<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
	</div>
</div>
<?php echo form_close(); ?>
