<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 12:29 PM
 */
?>
<footer style="background-color: #2f2f2f; color: white;" class="footer subscribe text-center">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 col-12">
				<br>
				<h3>I <strong>DON'T</strong> Spam!</h3>
				<p> I am just an individual trying to notify you about new uploads. If you
					wanna help a llama out. Consider subscribing to the news letter. :)</p>
			</div>
			<div class="col-sm-5 col-12">
				<br>
				<form>
					<div class="form-group">
						<label for="exampleInputEmail1" class="h3">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
							   placeholder="Enter email">
						<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
							else.
						</small>
					</div>
					<button type="submit" class="btn btn-outline-success">Subscribe</button>
				</form>
			</div>
		</div>
	</div>
	<div class="container">
		<br>
		<a href="" class="fb-ic">
			<i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
		</a>
		<a href="http://twitter.com/iamllamma" class="tw-ic">
			<i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
		</a>
		<a href="" class="gplus-ic">
			<i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
		</a>
		<a href="https://www.linkedin.com/in/sahil-shukla-b66766166/" class="li-ic">
			<i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
		</a>
		<a href="" class="ins-ic">
			<i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
		</a>
	</div>
	<br><br>
</footer>

<script>
	window.onload = () => {
		let el = document.querySelector('[alt="www.000webhost.com"]').parentNode.parentNode;
		el.parentNode.removeChild(el);
	}
</script>

<script src="https://use.fontawesome.com/b65c38cb85.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
</body>
</html>
