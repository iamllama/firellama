<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 12:29 PM
 */
?>
<html lang="en">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="http://cdn.ckeditor.com/4.11.2/full/ckeditor.js"></script>
	<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
		  integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
		  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cards.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		  integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
		  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/navy.css">
	<title>
		llamasec | Web Security and Programming Blog
	</title>
	<script>
		$(window).load(function () {
			$('.post-module').hover(function () {
				$(this).find('.description').stop().animate({
					height: "toggle",
					opacity: "toggle"
				}, 300);
			});
		});
	</script>
</head>
<body>

<div class="container">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="<?php echo base_url(); ?>"><img
				src="<?php echo base_url(); ?>assets/img/llamasec.png" alt="llamasec logo"
				id="navbar-brand"></a>
		<button id="navbar-toggler" class="navbar-brand navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span style="margin: auto;"><img src="
	<?php echo base_url(); ?>assets/img/llamasec.png" alt="llamasec logo"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto text-left">
				<li class="nav-item active">
					<h5 id="navcontent"><a class="nav-link ml-5" href="
	<?php echo base_url(); ?>">Home <span
								class="sr-only">(current)</span></a></h5>
				</li>
				<li class="nav-item">
					<h5 id="navcontent"><a class="nav-link ml-5" href="
	<?php echo base_url(); ?>posts">Blogs</a></h5>
				</li>
				<li class="nav-item">
					<h5 id="navcontent"><a class="nav-link ml-5" href="
	<?php echo base_url(); ?>about">About</a></h5>
				</li>
				<?php //if ($this->session->userdata('logged_in')): ?>
				<li class="nav-item">
					<h5 id="navcontent"><a class="nav-link ml-5" href="
	<?php //echo base_url() . 'posts/create' ?>">Create</a>
					</h5>
				</li>
				<li class="nav-item">
					<h5 id="navcontent"><a class="nav-link ml-5" href="
	<?php echo base_url(); ?>users/dashboard/">Dashboard</a>
					</h5>
				</li>
				<?php //endif; ?>
			</ul>
		</div>
	</nav>
</div>
<?php
	echo $this->session->flashdata("email_required");
?>
