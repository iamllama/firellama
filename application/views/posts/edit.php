<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/22/2019
 * Time: 1:40 PM
 */
?>

<h2><?= $title; ?></h2>
<?php echo validation_errors();?>

<?php echo form_open('posts/update/'.$post['id'])?>
<div class="form-group">
	<input type="hidden" name="id" value="<?php echo $post['id']?>">
	<label for="exampleInputEmail1">Title</label>
	<input value="<?php echo $post['title']?>" type="text" class="form-control" name="title" id="exampleInputEmail1" aria-describedby="emailHelp"
		   placeholder="Title">
</div>
<div class="form-group">
	<label for="exampleInputPassword1">Body</label>
	<textarea id="editor1" class="form-control" name="body" placeholder="Your content"><?php echo $post['body']?></textarea>
</div>
<div class="form-group">
	<label for="exampleInputPassword1">Excerpt</label>
	<input type="text" class="form-control" name="excerpt" placeholder="Excerpt">
</div>
<div class="form-group slug">
	<label for="exampleInputPassword1">Slug</label>
	<input type="text" class="form-control" value="<?php echo $post['slug'] ?>" name="slug" placeholder="Slug">
</div>
<div class="form-group">
	<label>Category</label>
	<select name="category_id" class="form-control">
		<?php foreach($categories as $category): ?>
			<option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option
		<?php endforeach; ?>
	</select>
</div>
<button type="submit" class="btn btn-primary">Submit</button>
<?php echo form_close() ?>
<script>
	CKEDITOR.replace('editor1');
</script>
