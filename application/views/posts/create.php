<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/22/2019
 * Time: 1:11 PM
 */
?>
<?php echo validation_errors(); ?>
<?php echo form_open('posts/create') ?>
<div class="container">
	<h2 class="text-center">Shabdo ko piro do</h2>
	<div class="form-group">
		<label for="Title">Title</label>
		<input type="text" class="form-control" name="title" aria-describedby="emailHelp"
			   placeholder="Title">
	</div>
	<div class="form-group ckeditor">
		<label for="exampleInputPassword1">Body</label>
		<textarea id="editor1" class="form-control" name="body" placeholder="Your content"></textarea>
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Excerpt</label>
		<input type="text" class="form-control" name="excerpt" placeholder="Excerpt">
	</div>
	<div class="form-group slug">
		<label for="exampleInputPassword1">Slug</label>
		<input type="text" class="form-control" name="slug" placeholder="Slug">
	</div>
	<div class="form-group">
		<label>Category</label>
		<select name="category_id" class="form-control">
			<?php foreach($categories as $category): ?>
			<option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option
			<?php endforeach; ?>
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</div>
<?php echo form_close(); ?>
<script>
	CKEDITOR.replace('editor1');
</script>
