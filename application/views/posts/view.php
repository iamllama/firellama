<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 1:34 PM
 */
?>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-8 col-lg-8">
			<h2><?php echo $post['title']; ?></h2>
			<small class="post-date">Created on <?php echo $post['created_at'] ?></small>
			<small class="post-date">Category<?php echo $post['name'] ?></small>
			<br>
			<div class="">
				<?php echo $post['body']; ?>
			</div>
		</div>
		<div class="hidden-sm hidden-xs col-md-4 col-lg-4">
			<h1 class="h1 text-center">Subscribe To Our Newsletter</h1>
			<img class="img-fluid" src="<?php echo base_url() . 'assets/img/sidebar.png'; ?>"
				 alt="subscribe to new letter">
			<br><br>
			<?php echo form_open('contact/subscribe') ?>
			<div class="form-group">
				<input placeholder="Email" class="form-control" type="text" name="email"
					   aria-label="subscribe to email">
			</div>
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
				   value="<?php echo $this->security->get_csrf_hash(); ?>">
			<button class="btn btn-outline-secondary" type="submit" name="submit">Subscribe!</button>
			<?php echo form_close(); ?>
		</div>
	</div>
	<?php
	if ($this->session->userdata('logged_in')):
		echo form_open(base_url() . 'posts/delete/' . $post['id']); ?>
		<input type="submit" value="delete" class="btn btn-danger">
		<?php echo form_close(); ?>
		<a class="btn" href="<?php echo base_url() . 'posts/edit/' . $post['slug']; ?>">Edit</a>
	<?php
	else:
		echo "can't see you logged in nibba";
	endif; ?>
</div>
