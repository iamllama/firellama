<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 12:51 PM
 */
?>

<div class="container">
	<div class="row">
		<?php foreach ($posts as $post): ?>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4 my-3 service1">
				<img class="img-fluid my-2"
					 src="<?php echo base_url() . 'assets/img/' . $post['featured_image']; ?>"
					 alt="<?php echo $post['title']; ?>">
				<h4><?php echo $post['title']; ?></h4>
				<p class="excerpt"><?php echo $post['excerpt']; ?></p>
				<a href="<?php echo 'posts/'.$post['slug']; ?>" class="cta">Read More <span class="ti-angle-right"></a>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<br><br><br><br>
