<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 12:49 PM
 */

class Posts extends CI_Controller
{
	public function index()
	{
		$data['title'] = "Latest Posts";

		$data['posts'] = $this->post_model->get_posts();

		$this->load->view('templates/header');
		$this->load->view('posts/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug = NULL)
	{
		$data['post'] = $this->post_model->get_posts($slug);
		if (empty($data['post'])) {
			show_404();
		}
		$data['title'] = $data['post']['title'];

		$this->load->view('templates/header');
		$this->load->view('posts/view', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url() . 'users/login');
		} else {
			$data['title'] = "Shabdo ko piro do";
			$data['categories'] = $this->post_model->get_categories();

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('body', 'Body', 'required');
			$this->form_validation->set_rules('slug', 'Slug', 'required');
			$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');

			if ($this->form_validation->run() === FALSE) {
				$this->load->view('templates/header');
				$this->load->view('posts/create', $data);
				$this->load->view('templates/footer');
			} else {
				$this->post_model->set_post();
				redirect(base_url() . '/posts/');
			}
		}
	}

	public function delete($id)
	{
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url() . 'users/login');
		} else {
			$this->post_model->delete_post($id);
			redirect(base_url() . 'posts/	');
		}
	}

	public function edit($slug)
	{
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url() . 'users/login');
		} else {
			$data['post'] = $this->post_model->get_posts($slug);

			if (empty($data['post'])) {
				show_404();
			}
			$data['title'] = 'Edit posts';
			$data['categories'] = $this->post_model->get_categories();

			$this->load->view('templates/header');
			$this->load->view('posts/edit', $data);
			$this->load->view('templates/footer');
		}
	}

	public function update($id)
	{
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url() . 'users/login');
		} else {
			$this->post_model->update_post($id);
			redirect(base_url() . 'posts');
		}
	}
}
