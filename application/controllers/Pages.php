<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/20/2019
 * Time: 12:26 PM
 */
class Pages extends CI_Controller {
	public function view($page = 'home'){
		if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
			show_404();
		}
//		caching page
		$this->output->cache(525600);
//		Delete cache on Content Update
//		$this->output->delete_cache();

		$data['title'] = ucfirst($page);
		$this->load->view('templates/header');
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer');
	}
}
