<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/27/2019
 * Time: 8:59 PM
 */
class Contact extends CI_Controller
{
	public function subscribe()
	{
		$this->form_validation->set_rules('email', 'Email', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->flashdata('Email is required');
		} else {
			$this->contact_model->subscribe_now();
		}
	}

}
