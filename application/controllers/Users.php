<?php
/**
 * Created by PhpStorm.
 * User: llama
 * Date: 1/22/2019
 * Time: 2:58 PM
 */

class Users extends CI_Controller
{

	public function login()
	{
		$failed_attempts = 0;
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header');
			$this->load->view('users/login');
			$this->load->view('templates/footer');
		} else {
			$username = $this->input->post('username');
			$password = sha1($this->input->post('password'));

			$user_id = $this->user_model->login($username, $password);
			if ($user_id) {
				// create login session
				$this->session->set_userdata('logged_in', 'yoyoyo');
				$this->session->set_flashdata('user_loggedin', 'You are now logged in');
				$failed_attempts = 0;
				redirect(base_url() . 'users/dashboard/');
			} else {
				$failed_attempts++;
				if ($failed_attempts >= 5) {
					echo "Yo done fucked up boy. No more logging in for you.";
				}
				$this->session->set_flashdata('login_failed', 'You done fucked up');
				redirect(base_url() . 'users/login');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		redirect(base_url());
	}

	public function dashboard()
	{
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url() . 'users/login');
		}
		$this->load->view('templates/header');
		$this->load->view('users/dashboard');
		$this->load->view('templates/footer');
	}
}
