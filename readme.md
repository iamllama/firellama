<p align="center"><img src="https://llamasec.tk/assets/img/me.jpg"></p>

<p align="center">
<a href="https://llamasec.tk"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://llamasec.tk"><img src="https://poser.pugx.org/laravel/framework/v/unstable.svg" alt="Latest Stable Version"></a>
</p>

## About TechPolygon
This blog is dedicated to encompass my journey in the Infosec and the Programming industry and hopefully help someone 
out with the troubles that I once faced. I hope to see you join me and experience the adventures of a script kiddie to 
become a pro.

- http://www.llamasec.tk


## Understanding the Code
I will try my level best to not fuck up the only piece of document that'll help me in the future but 
it all boils down to how much they wanna kill myself that particular day of document writing. 


## Setting up the development environment
Just run setup_environment.ps1 as administrator and magic will happen. If it doesn't work it's because your computer is
a [muggle](https://en.wikipedia.org/wiki/Muggle).

- git clone https://gitlab.com/iamllama/llamasec.git
- Place the repo in <em>htdocs</em> folder

### Installing Dependencies Globally
Normally you would require composer to do this, but FUCKING CODEIGNITER is it's gorgeous developers have the whole thing
packaged in a zip, every dependency, like I am not kidding bro, if I see em I am buying the MF's a beer. 
        
- CKeditor installation
    - Download the following links 
    	- [CKEditor](http://pastebin.com/fkK9e0RR)
    	- [CKFinder](http://pastebin.com/SvyypmX4)
    - Copy the files you just downloaded into your Application/libraries folder
    - Download the ckeditor helper here
    	- [CKHelper](http://pastebin.com/Cd3GqYbx)
	- Copy the last file in application/helper folder as ckeditor_helper.php
	- Download [CKEditor Controller](http://pastebin.com/UD0bB9ig)
	- Copy the controller in your application/controllers folder as ``ckeditor.php``
	- Download the main [CKEditor](http://ckeditor.com/download/)
	- Copy the ckeditor folder you just download into your asset folder (if you want you can also download the ckfinder project and put it in the same folder)
	- Add these line of js to your view file (adjust the path):
		``<script type="text/javascript" src="/asset/ckeditor/ckeditor.js"></script>
          <script type="text/javascript" src="/asset/ckfinder/ckfinder.js"></script>``
    - In your controller add this php code and adjust the path:
    - ``$this->load->library('ckeditor');``
        ``$this->load->library('ckfinder');``
        ``$this->ckeditor->basePath = base_url().'asset/ckeditor/';``
        ``$this->ckeditor->config['toolbar'] = array(array( 'Source', '-', 'Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList' )``
                                                            );
        ``$this->ckeditor->config['language'] = 'it';``
        ``$this->ckeditor->config['width'] = '730px';``
        ``$this->ckeditor->config['height'] = '300px';``            
        ``//Add Ckfinder to Ckeditor``
        ``$this->ckfinder->SetupCKEditor($this->ckeditor,'../../asset/ckfinder/'); ``
    - In your view print the editor with:
    	``echo $this->ckeditor->editor("textarea name","default textarea value");``
	

### DataBase Setup
- Create a database called <em>firellama</em> for local production but in production we've got 
	<em>id8356573_firellama</em> in compliance to the **.env** file's default database name. 
- Use the MySQL setup file to fill the tables

## Understanding the Project Structure
The designing is mainly based inside the **views** directory. The Views directory further has 4 <em>custom created 
folders</em>. 
- Application 
    - It has all the directories and dependencies in it
- Config 
    - Config folder has all the configs (big surprise). 
    	- Database.php
    		 - Setup your DB connection here
- Views 
    - You'll figure it out

    
**MOTIVATIONAL NOTE** You're a good developer.

## VCS (Version Control System) Guide
- All the maintainers are entitled to one branch. (So just me then)
- Every branch should be named sensibly. 
- Don't bombard the system with merge requests. 
- All the merge requests should be handled by one person. (me again)
- Commit small, push big. 

## Security Vulnerabilities

If you discover a security vulnerability within llamasec or codeigniter, please send an e-mail to Sahil Shukla via [sahil@llamasec.tk](mailto:sahil@llamasec.tk). All security vulnerabilities will be promptly addressed.

## License

The license is restricted as shit, except for Sahil Shukla no one is allowed to read this fucking code
you dumb bitches.

Copyleft &copy; LLAMASEC 2018 | All rights reserved
